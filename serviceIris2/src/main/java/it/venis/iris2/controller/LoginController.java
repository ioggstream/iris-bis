package it.venis.iris2.controller;

import java.util.Calendar;

import it.venis.iris2.db.utils.CDbUtils;
import it.venis.iris2.security.CGestioneUtente;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;


@RestController
public class LoginController {

	public LoginController() {
		// TODO Auto-generated constructor stub
	}

	@CrossOrigin (origins = "*" )
    @RequestMapping(value = "/echouser",  produces={"application/json"},  method={RequestMethod.GET, RequestMethod.POST})
    public String echouser(HttpServletRequest request)
    {
		String sJson = "";
		CGestioneUtente gu = new CGestioneUtente();
		sJson = gu.getCurrentUser();
		
		String theBearer = request.getHeader("Authorization");
		if (gu.sGetDatiBySpid(theBearer)) 
			sJson = gu.getsNome();
		else
			if (gu.sGetDatiByDiMe(theBearer))
				sJson = gu.getsNome();
			else
				if (gu.sGetDatiByUncredited(theBearer))
					sJson = gu.getsNome();
		
        return sJson;     
    }

	
	@CrossOrigin (origins = "*" )
    @RequestMapping(value = "/abilitato",  produces={"application/json"},  method={RequestMethod.GET, RequestMethod.POST})
    public String abilitato(HttpServletRequest request, @RequestParam(value="theSegn", required=false) Long theSegn)
    {
		if (theSegn != null) if (new CGestioneUtente().bControllaAbilitUtente(theSegn)) return "{\"abilitato\": \"yes\"}";
		return "{\"abilitato\": \"no\"}";
    }
	
	// ----- controllo di appartenenza di una segnalazione
	@CrossOrigin (origins = "*" )
    @RequestMapping(value = "/checkmysegn",  produces={"application/json"},  method={RequestMethod.GET, RequestMethod.POST})
    public String checkMySegn(HttpServletRequest request, @RequestParam(value="theSegn", required=false) Long theSegn, @RequestBody (required = false) String body)
    {
		CDbUtils dbu = new CDbUtils();		
		if (theSegn == null && body != null) 
		{
			JsonObject jsonObj = (JsonObject) new JsonParser().parse(body);
			theSegn = jsonObj.get("theSegn").getAsLong();
		}
		if (theSegn != null)
		{
			String sIdUtente = new CGestioneUtente().getCurrentUserIrisID();
			if (sIdUtente != null)
			{
				if (dbu.checkMia (theSegn, Long.parseLong(sIdUtente)))
					return "{\"vedi\": \"yes\"}";
			}
		}
		return "{\"vedi\": \"no\"}";
    }

}
