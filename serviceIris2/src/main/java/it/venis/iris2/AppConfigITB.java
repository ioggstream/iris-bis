package it.venis.iris2;

import javax.sql.DataSource;

import org.apache.commons.dbcp.BasicDataSource;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.mapper.MapperScannerConfigurer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
public class AppConfigITB {
	// il primo DataSource è marcato come @Primary per essere gestito dalla autoconfiguration di Spring...
	@Bean (name = "itbdb")	
    public  DataSource dsITB() {
 		BasicDataSource dataSource = new BasicDataSource();
        dataSource.setDriverClassName("oracle.jdbc.driver.OracleDriver");
        dataSource.setUrl("jdbc:oracle:thin:@SERVER:PORT/SERVICE"); 
        dataSource.setUsername("USER");
        dataSource.setPassword("PASSWORD");
       return dataSource;
   }

   
   @Bean (name = "sqlSessionFactoryITB")
   public SqlSessionFactory sqlSessionFactory() throws Exception {
      SqlSessionFactoryBean sessionFactory = new SqlSessionFactoryBean();
      sessionFactory.setDataSource(dsITB());     
      return sessionFactory.getObject();
   }
   
   @Bean
   public MapperScannerConfigurer mapperScannerConfigurerITB() {
       MapperScannerConfigurer configurer = new MapperScannerConfigurer();
       configurer.setBasePackage("it.venis.iris2.oggettidbiris");
       configurer.setSqlSessionFactoryBeanName("sqlSessionFactoryITB");
       return configurer;
   }
}
