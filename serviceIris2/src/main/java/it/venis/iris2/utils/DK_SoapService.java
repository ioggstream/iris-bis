package it.venis.iris2.utils;

import it.venis.iris2.AppConfigDbIris;
import it.venis.iris2.db.utils.CDbUtils;
import it.venis.iris2.oggettidbiris.VfixFotografie;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.Proxy;
import java.net.URL;
import java.net.URLConnection;
import java.util.Base64;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;

public class DK_SoapService {

	Logger logger = Logger.getLogger("MyLogDK");  
	FileHandler fh;
	java.util.ResourceBundle mybundle = java.util.ResourceBundle.getBundle("application");

	private static String wsURL = "http://localhost/PMService/DK_PMService.svc";
	private static String username = "iris";
	private static String password = "PASSWORD";


	public DK_SoapService() {
		
		 try {
			fh = new FileHandler(mybundle.getString("logging.coop.fileDK"),true);
		    logger.addHandler(fh);
		    SimpleFormatter formatter = new SimpleFormatter();  
		    fh.setFormatter(formatter); 

		} catch (SecurityException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}  
	}
	
	protected void finalize() throws Throwable {
       super.finalize();
       logger.removeHandler(fh);
       fh.close();
	 }
	
	
	private String SoapService(String SOAPAction, String SOAPBody) 
			{
		//Code to make a webservice HTTP request
		
		try {
			System.setProperty("proxySet", "true");		
			System.setProperty("https.proxyHost", "172.22.10.116");
			System.setProperty("https.proxyPort", "80");
			System.setProperty("https.nonProxyHosts", "localhost|127.0.0.1|10.*|172.22.*");

			
			String responseString = "";
			String outputString = "";
			URL url = new URL(wsURL);
			URLConnection connection = url.openConnection(Proxy.NO_PROXY);
			HttpURLConnection httpConn = (HttpURLConnection)connection;

				String userpass = username + ":" + password;
				String basicAuth = "Basic " + new String(Base64.getEncoder().encode(userpass.getBytes()));
				connection.setRequestProperty ("Authorization", basicAuth);

			ByteArrayOutputStream bout = new ByteArrayOutputStream();
			String xmlInput =
			"<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\" xmlns:dk=\"http://schemas.datacontract.org/2004/07/DK_PM\" xmlns:arr=\"http://schemas.microsoft.com/2003/10/Serialization/Arrays\"> " + 
			" <soapenv:Header/> " +
			" <soapenv:Body> " +
			SOAPBody +
			" </soapenv:Body> " +
			" </soapenv:Envelope> ";		
			 
			byte[] buffer = new byte[xmlInput.length()];
			buffer = xmlInput.getBytes();
			bout.write(buffer);
			byte[] b = bout.toByteArray();

			httpConn.setRequestProperty("Content-Length",
			String.valueOf(b.length));
			httpConn.setRequestProperty("Content-Type", "text/xml; charset=utf-8");
			httpConn.setRequestProperty("SOAPAction", SOAPAction);
			httpConn.setRequestMethod("POST");
			httpConn.setDoOutput(true);
			httpConn.setDoInput(true);
			OutputStream out = httpConn.getOutputStream();
			//Write the content of the request to the outputstream of the HTTP Connection.
			out.write(b);
			out.close();
			//Ready with sending the request.
			 
			//Read the response.
			InputStreamReader isr =
			new InputStreamReader(httpConn.getInputStream());
			BufferedReader in = new BufferedReader(isr);
			 
			//Write the SOAP message response to a String.
			while ((responseString = in.readLine()) != null) {
				outputString = outputString + responseString;
			}
			return outputString;
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.info("errore: " + e.getMessage());
			logger.info("SOAPBody: " + SOAPBody);
		} catch (ProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.info("errore: " + e.getMessage());
			logger.info("SOAPBody: " + SOAPBody);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.info("errore: " + e.getMessage());
			logger.info("SOAPBody: " + SOAPBody);
		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.info("errore: " + e.getMessage());
			logger.info("SOAPBody: " + SOAPBody);
		}
		
		return "";
	}
	
	public String IsServiceOnline() throws MalformedURLException,
	IOException {
		String SOAPAction = "http://tempuri.org/IDK_PMService/IsServiceOnline";
		String SOAPBody = " <tem:IsServiceOnline/> ";
		String outputString = SoapService( SOAPAction,  SOAPBody); 
		System.out.println(outputString);
		return tagXMLValue(outputString,"IsServiceOnlineResult");
	}

	public String SendRequest(String idCoordinatore) {
		String SOAPAction = "http://tempuri.org/IDK_PMService/SendRequest";
		String SOAPBody = " <tem:SendRequest> " +
                          " <tem:RequestName>" + idCoordinatore + "</tem:RequestName> " +
                          " </tem:SendRequest> ";
		try {
 		String outputString = SoapService( SOAPAction,  SOAPBody); 
		System.out.println(outputString);		
		return tagXMLValue(outputString,"SendRequestResult");
		}
		catch (Exception e) {
			// TODO Auto-generated catch block
				e.printStackTrace();
				logger.info("errore: " + e.getMessage());
				return "";
			}
	}

	
	public String getCreateRequest(
			it.venis.iris2.irisinterface.DatiIris datiNotifica
			) 
		{
			String sRet = "";
			String SOAPAction = "http://tempuri.org/IDK_PMService/CreateRequest";
			String SOAPBody = 
			"       <tem:CreateRequest> " +
			"          <tem:request> " +
			"             <dk:Chiave_iris>"+ datiNotifica.getChiaveIris().toString() +"</dk:Chiave_iris> " +
			"             <dk:Codcivico>"+ datiNotifica.getCodcivico().toString() +"</dk:Codcivico> " +
			"             <dk:Codvia>"+ datiNotifica.getCodvia().toString() +"</dk:Codvia> " +
			"             <dk:Denominazione></dk:Denominazione> " +
			"             <dk:Foto> ";
			
			for (byte bIdx = 0; bIdx < datiNotifica.getFoto().getUrl().size(); bIdx++)
				SOAPBody += "    <arr:string>"+ datiNotifica.getFoto().getUrl().get(bIdx) +"</arr:string> ";
	
			
			String tmpIndicazioni = datiNotifica.getIndicazioni();
			if (tmpIndicazioni.isEmpty())
				tmpIndicazioni = "-";
			
			SOAPBody += " </dk:Foto> " +
			"             <dk:Indicazioni>"+ tmpIndicazioni +"</dk:Indicazioni> " +
			"             <dk:Indirizzo>"+ datiNotifica.getIndirizzo().toString() +"</dk:Indirizzo> " +
			"             <dk:Lat>"+ datiNotifica.getLat().toString() +"</dk:Lat> " +
			"             <dk:Lettera>"+ datiNotifica.getLettera().toString() +"</dk:Lettera> " +
			"             <dk:Long>"+ datiNotifica.getLong().toString() +"</dk:Long> " +
			"             <dk:Municipalita>"+ datiNotifica.getMunicipalita().toString() +"</dk:Municipalita> " +
			"             <dk:Oggetto>"+ datiNotifica.getOggetto().toString() +"</dk:Oggetto> " +
			"             <dk:Specie></dk:Specie> " +
			"             <dk:Tipologia>"+ datiNotifica.getTipologia() +"</dk:Tipologia> " +
			"             <dk:Utente> " +
			"                <dk:Cognome></dk:Cognome> " +
			"                <dk:Email></dk:Email> " +
			"                <dk:Nome></dk:Nome> " +
			"                <dk:Tag></dk:Tag> " +
			"                <dk:Telefono></dk:Telefono> " +
			"             </dk:Utente> " +
			"          </tem:request> " +
			"       </tem:CreateRequest> ";
			try {
			
				String outputString = SoapService( SOAPAction,  SOAPBody); 
				System.out.println(outputString);
				sRet = "";
		
				sRet = tagXMLValue(outputString,"RequestName");
				} catch (Exception e) {
				// TODO Auto-generated catch block
					e.printStackTrace();
					logger.info("errore: " + e.getMessage());
				}
		
				return sRet; 
	}
	
	 public String  tagXMLValue(String xmlstring, String tagName)  {
		 DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		 DocumentBuilder builder;
		 String resp = "";
		try {
			builder = factory.newDocumentBuilder();
			 InputStream stream = new ByteArrayInputStream(xmlstring.getBytes("UTF-8"));
			 InputSource is = new InputSource(stream);
			 Document doc = builder.parse(is);
			 
		        String rootNode =  doc.getDocumentElement().getNodeName();
		        NodeList bookslist = doc.getElementsByTagName(rootNode);
		        String servername = (String) ((Element) bookslist.item(0)).getElementsByTagName(tagName).
		                        item(0).getChildNodes().item(0).getNodeValue();
		        resp =servername.trim();
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.info("errore: " + e.getMessage());
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.info("errore: " + e.getMessage());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.info("errore: " + e.getMessage());
		}
 		 return resp;
	    }	
	
//	public static void main(String[] args)
//	{
//		String res = "??";
//		try {
//			// res = IsServiceOnline();	// -> OK: Servizio DK_PMService attivo
// 		     String[] Foto = {""};
//			 res = getCreateRequest(
//					null
//					);	// es. res = "Ticket_00301_2018";
// 		    
//			if (res.contains("Ticket")) {
//				res = SendRequest(res);	// -> 0 = OK
//			}	
//			res = "RISP." + res;
//		} catch (MalformedURLException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}	
//		System.out.println(res);
//	}
	 
	public String inviaDati888(it.venis.iris2.irisinterface.DatiIris datiNotifica)
	{
		String sRis = "";
		String res = "??";
		try {
			// res = IsServiceOnline();	// -> OK: Servizio DK_PMService attivo
 		     String[] Foto = {""};
			 res = getCreateRequest(
					 datiNotifica
					);	// es. res = "Ticket_00301_2018";
			 sRis = res;
			 logger.info ("\n\n");
			logger.info ("****************************************");			 
			logger.info ("****************************************");
			logger.info("segnalazione: " + datiNotifica.getChiaveIris());
			logger.info("dati inviati: " + sRis);
			if (!res.contentEquals("")) {
				res = SendRequest(res);	// -> 0 = OK
				logger.info("res: " + res);
				if (!res.equals("0")) sRis =""; 
			}	
			res = "RISP." + res;
			logger.info("risposta: " + sRis);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.info("errore: " + e.getMessage());
			sRis ="";
		}	
		logger.info ("****************************************");
		logger.info ("****************************************");
		logger.info ("\n\n");
		System.out.println(res);
		return sRis;
	}

	
	public String NotifyRequest(String idCoordinatore) throws MalformedURLException,
	IOException {
		String SOAPAction = "http://tempuri.org/IDK_PMService/NotifyRequest";
		String SOAPBody = " <tem:NotifyRequest> " +
                " <tem:RequestName>" + idCoordinatore + "</tem:RequestName> " +
                " </tem:NotifyRequest> ";
		String outputString = SoapService( SOAPAction,  SOAPBody); 
		System.out.println(outputString);
		return tagXMLValue(outputString,"NotifyRequestResult");
	}
	
	// cos'è chiave coop? Io ce l'ho...
	public  String updateRequest(Long nId888, String[] Foto, String sNote)
  			throws MalformedURLException, IOException {
		
		String sPathFoto = "https://irisadmin.comune.venezia.it/foto/";
		
  		String SOAPAction = "http://tempuri.org/IDK_PMService/UpdateRequest";
		
  		String SOAPBody = 
  				"       <tem:UpdateRequest> " +
  				"         <tem:RequestName>"+ String.format("%010d", nId888) +"</tem:RequestName> " +
  				"          <tem:request> " +
  				"             <dk:Foto> ";
				for (int i=0; i<Foto.length; i++)	{
						SOAPBody += "    <arr:string>"+ sPathFoto + Foto[i]+"</arr:string> ";
	          	}
		  		
  				SOAPBody += " </dk:Foto> " +
  				"             <dk:Note>"+ sNote +"</dk:Note> " +
  				"             <dk:Utente> " +
  				"                <dk:Cognome></dk:Cognome> " +
  				"                <dk:Email></dk:Email> " +
  				"                <dk:Nome></dk:Nome> " +
  				"                <dk:Tag></dk:Tag> " +
  				"                <dk:Telefono></dk:Telefono> " +
  				"             </dk:Utente> " +
  				"          </tem:request> " +
  				"       </tem:UpdateRequest> ";
  		String outputString = SoapService( SOAPAction,  SOAPBody); 
  		System.out.println(outputString);
  		
  		/* salviamo qui il log, utilizzando xml */
  		CDbUtils dbu = new CDbUtils();
  		dbu.bSalvaNote888(SOAPBody);  // salvo il json
  		
  		return tagXMLValue(outputString,"UpdateRequestResult");
  	}
	
	
	// ---
	public String aggiornaNote888(Long lId, String[] Foto, String sNote)
	{
		String sRis = "";
		String res = "??";
		try {
			
			// leggere id di 888 dal db...
			Long nId888 = lId;
 			res = updateRequest(nId888, Foto, sNote);
 			res = "RISP." + res;
 			res = NotifyRequest(String.format("%010d", nId888));	// -> 0 = OK
 
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			sRis ="";
		}	
		System.out.println(res);
		return sRis;
	}
}
