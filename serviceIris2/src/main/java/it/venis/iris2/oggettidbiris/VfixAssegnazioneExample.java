package it.venis.iris2.oggettidbiris;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class VfixAssegnazioneExample {
    /**
	 * This field was generated by MyBatis Generator. This field corresponds to the database table VFIX_ASSEGNAZIONE
	 * @mbggenerated  Thu Jun 20 11:01:45 CEST 2019
	 */
	protected String orderByClause;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database table VFIX_ASSEGNAZIONE
	 * @mbggenerated  Thu Jun 20 11:01:45 CEST 2019
	 */
	protected boolean distinct;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database table VFIX_ASSEGNAZIONE
	 * @mbggenerated  Thu Jun 20 11:01:45 CEST 2019
	 */
	protected List<Criteria> oredCriteria;

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table VFIX_ASSEGNAZIONE
	 * @mbggenerated  Thu Jun 20 11:01:45 CEST 2019
	 */
	public VfixAssegnazioneExample() {
		oredCriteria = new ArrayList<Criteria>();
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table VFIX_ASSEGNAZIONE
	 * @mbggenerated  Thu Jun 20 11:01:45 CEST 2019
	 */
	public void setOrderByClause(String orderByClause) {
		this.orderByClause = orderByClause;
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table VFIX_ASSEGNAZIONE
	 * @mbggenerated  Thu Jun 20 11:01:45 CEST 2019
	 */
	public String getOrderByClause() {
		return orderByClause;
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table VFIX_ASSEGNAZIONE
	 * @mbggenerated  Thu Jun 20 11:01:45 CEST 2019
	 */
	public void setDistinct(boolean distinct) {
		this.distinct = distinct;
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table VFIX_ASSEGNAZIONE
	 * @mbggenerated  Thu Jun 20 11:01:45 CEST 2019
	 */
	public boolean isDistinct() {
		return distinct;
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table VFIX_ASSEGNAZIONE
	 * @mbggenerated  Thu Jun 20 11:01:45 CEST 2019
	 */
	public List<Criteria> getOredCriteria() {
		return oredCriteria;
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table VFIX_ASSEGNAZIONE
	 * @mbggenerated  Thu Jun 20 11:01:45 CEST 2019
	 */
	public void or(Criteria criteria) {
		oredCriteria.add(criteria);
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table VFIX_ASSEGNAZIONE
	 * @mbggenerated  Thu Jun 20 11:01:45 CEST 2019
	 */
	public Criteria or() {
		Criteria criteria = createCriteriaInternal();
		oredCriteria.add(criteria);
		return criteria;
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table VFIX_ASSEGNAZIONE
	 * @mbggenerated  Thu Jun 20 11:01:45 CEST 2019
	 */
	public Criteria createCriteria() {
		Criteria criteria = createCriteriaInternal();
		if (oredCriteria.size() == 0) {
			oredCriteria.add(criteria);
		}
		return criteria;
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table VFIX_ASSEGNAZIONE
	 * @mbggenerated  Thu Jun 20 11:01:45 CEST 2019
	 */
	protected Criteria createCriteriaInternal() {
		Criteria criteria = new Criteria();
		return criteria;
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table VFIX_ASSEGNAZIONE
	 * @mbggenerated  Thu Jun 20 11:01:45 CEST 2019
	 */
	public void clear() {
		oredCriteria.clear();
		orderByClause = null;
		distinct = false;
	}

	/**
	 * This class was generated by MyBatis Generator. This class corresponds to the database table VFIX_ASSEGNAZIONE
	 * @mbggenerated  Thu Jun 20 11:01:45 CEST 2019
	 */
	protected abstract static class GeneratedCriteria {
		protected List<Criterion> criteria;

		protected GeneratedCriteria() {
			super();
			criteria = new ArrayList<Criterion>();
		}

		public boolean isValid() {
			return criteria.size() > 0;
		}

		public List<Criterion> getAllCriteria() {
			return criteria;
		}

		public List<Criterion> getCriteria() {
			return criteria;
		}

		protected void addCriterion(String condition) {
			if (condition == null) {
				throw new RuntimeException("Value for condition cannot be null");
			}
			criteria.add(new Criterion(condition));
		}

		protected void addCriterion(String condition, Object value,
				String property) {
			if (value == null) {
				throw new RuntimeException("Value for " + property
						+ " cannot be null");
			}
			criteria.add(new Criterion(condition, value));
		}

		protected void addCriterion(String condition, Object value1,
				Object value2, String property) {
			if (value1 == null || value2 == null) {
				throw new RuntimeException("Between values for " + property
						+ " cannot be null");
			}
			criteria.add(new Criterion(condition, value1, value2));
		}

		public Criteria andPtrSegnalazioneIsNull() {
			addCriterion("PTR_SEGNALAZIONE is null");
			return (Criteria) this;
		}

		public Criteria andPtrSegnalazioneIsNotNull() {
			addCriterion("PTR_SEGNALAZIONE is not null");
			return (Criteria) this;
		}

		public Criteria andPtrSegnalazioneEqualTo(Long value) {
			addCriterion("PTR_SEGNALAZIONE =", value, "ptrSegnalazione");
			return (Criteria) this;
		}

		public Criteria andPtrSegnalazioneNotEqualTo(Long value) {
			addCriterion("PTR_SEGNALAZIONE <>", value, "ptrSegnalazione");
			return (Criteria) this;
		}

		public Criteria andPtrSegnalazioneGreaterThan(Long value) {
			addCriterion("PTR_SEGNALAZIONE >", value, "ptrSegnalazione");
			return (Criteria) this;
		}

		public Criteria andPtrSegnalazioneGreaterThanOrEqualTo(Long value) {
			addCriterion("PTR_SEGNALAZIONE >=", value, "ptrSegnalazione");
			return (Criteria) this;
		}

		public Criteria andPtrSegnalazioneLessThan(Long value) {
			addCriterion("PTR_SEGNALAZIONE <", value, "ptrSegnalazione");
			return (Criteria) this;
		}

		public Criteria andPtrSegnalazioneLessThanOrEqualTo(Long value) {
			addCriterion("PTR_SEGNALAZIONE <=", value, "ptrSegnalazione");
			return (Criteria) this;
		}

		public Criteria andPtrSegnalazioneIn(List<Long> values) {
			addCriterion("PTR_SEGNALAZIONE in", values, "ptrSegnalazione");
			return (Criteria) this;
		}

		public Criteria andPtrSegnalazioneNotIn(List<Long> values) {
			addCriterion("PTR_SEGNALAZIONE not in", values, "ptrSegnalazione");
			return (Criteria) this;
		}

		public Criteria andPtrSegnalazioneBetween(Long value1, Long value2) {
			addCriterion("PTR_SEGNALAZIONE between", value1, value2,
					"ptrSegnalazione");
			return (Criteria) this;
		}

		public Criteria andPtrSegnalazioneNotBetween(Long value1, Long value2) {
			addCriterion("PTR_SEGNALAZIONE not between", value1, value2,
					"ptrSegnalazione");
			return (Criteria) this;
		}

		public Criteria andPtrRefIsNull() {
			addCriterion("PTR_REF is null");
			return (Criteria) this;
		}

		public Criteria andPtrRefIsNotNull() {
			addCriterion("PTR_REF is not null");
			return (Criteria) this;
		}

		public Criteria andPtrRefEqualTo(Integer value) {
			addCriterion("PTR_REF =", value, "ptrRef");
			return (Criteria) this;
		}

		public Criteria andPtrRefNotEqualTo(Integer value) {
			addCriterion("PTR_REF <>", value, "ptrRef");
			return (Criteria) this;
		}

		public Criteria andPtrRefGreaterThan(Integer value) {
			addCriterion("PTR_REF >", value, "ptrRef");
			return (Criteria) this;
		}

		public Criteria andPtrRefGreaterThanOrEqualTo(Integer value) {
			addCriterion("PTR_REF >=", value, "ptrRef");
			return (Criteria) this;
		}

		public Criteria andPtrRefLessThan(Integer value) {
			addCriterion("PTR_REF <", value, "ptrRef");
			return (Criteria) this;
		}

		public Criteria andPtrRefLessThanOrEqualTo(Integer value) {
			addCriterion("PTR_REF <=", value, "ptrRef");
			return (Criteria) this;
		}

		public Criteria andPtrRefIn(List<Integer> values) {
			addCriterion("PTR_REF in", values, "ptrRef");
			return (Criteria) this;
		}

		public Criteria andPtrRefNotIn(List<Integer> values) {
			addCriterion("PTR_REF not in", values, "ptrRef");
			return (Criteria) this;
		}

		public Criteria andPtrRefBetween(Integer value1, Integer value2) {
			addCriterion("PTR_REF between", value1, value2, "ptrRef");
			return (Criteria) this;
		}

		public Criteria andPtrRefNotBetween(Integer value1, Integer value2) {
			addCriterion("PTR_REF not between", value1, value2, "ptrRef");
			return (Criteria) this;
		}

		public Criteria andDataIsNull() {
			addCriterion("DATA is null");
			return (Criteria) this;
		}

		public Criteria andDataIsNotNull() {
			addCriterion("DATA is not null");
			return (Criteria) this;
		}

		public Criteria andDataEqualTo(Date value) {
			addCriterion("DATA =", value, "data");
			return (Criteria) this;
		}

		public Criteria andDataNotEqualTo(Date value) {
			addCriterion("DATA <>", value, "data");
			return (Criteria) this;
		}

		public Criteria andDataGreaterThan(Date value) {
			addCriterion("DATA >", value, "data");
			return (Criteria) this;
		}

		public Criteria andDataGreaterThanOrEqualTo(Date value) {
			addCriterion("DATA >=", value, "data");
			return (Criteria) this;
		}

		public Criteria andDataLessThan(Date value) {
			addCriterion("DATA <", value, "data");
			return (Criteria) this;
		}

		public Criteria andDataLessThanOrEqualTo(Date value) {
			addCriterion("DATA <=", value, "data");
			return (Criteria) this;
		}

		public Criteria andDataIn(List<Date> values) {
			addCriterion("DATA in", values, "data");
			return (Criteria) this;
		}

		public Criteria andDataNotIn(List<Date> values) {
			addCriterion("DATA not in", values, "data");
			return (Criteria) this;
		}

		public Criteria andDataBetween(Date value1, Date value2) {
			addCriterion("DATA between", value1, value2, "data");
			return (Criteria) this;
		}

		public Criteria andDataNotBetween(Date value1, Date value2) {
			addCriterion("DATA not between", value1, value2, "data");
			return (Criteria) this;
		}

		public Criteria andValidaIsNull() {
			addCriterion("VALIDA is null");
			return (Criteria) this;
		}

		public Criteria andValidaIsNotNull() {
			addCriterion("VALIDA is not null");
			return (Criteria) this;
		}

		public Criteria andValidaEqualTo(String value) {
			addCriterion("VALIDA =", value, "valida");
			return (Criteria) this;
		}

		public Criteria andValidaNotEqualTo(String value) {
			addCriterion("VALIDA <>", value, "valida");
			return (Criteria) this;
		}

		public Criteria andValidaGreaterThan(String value) {
			addCriterion("VALIDA >", value, "valida");
			return (Criteria) this;
		}

		public Criteria andValidaGreaterThanOrEqualTo(String value) {
			addCriterion("VALIDA >=", value, "valida");
			return (Criteria) this;
		}

		public Criteria andValidaLessThan(String value) {
			addCriterion("VALIDA <", value, "valida");
			return (Criteria) this;
		}

		public Criteria andValidaLessThanOrEqualTo(String value) {
			addCriterion("VALIDA <=", value, "valida");
			return (Criteria) this;
		}

		public Criteria andValidaLike(String value) {
			addCriterion("VALIDA like", value, "valida");
			return (Criteria) this;
		}

		public Criteria andValidaNotLike(String value) {
			addCriterion("VALIDA not like", value, "valida");
			return (Criteria) this;
		}

		public Criteria andValidaIn(List<String> values) {
			addCriterion("VALIDA in", values, "valida");
			return (Criteria) this;
		}

		public Criteria andValidaNotIn(List<String> values) {
			addCriterion("VALIDA not in", values, "valida");
			return (Criteria) this;
		}

		public Criteria andValidaBetween(String value1, String value2) {
			addCriterion("VALIDA between", value1, value2, "valida");
			return (Criteria) this;
		}

		public Criteria andValidaNotBetween(String value1, String value2) {
			addCriterion("VALIDA not between", value1, value2, "valida");
			return (Criteria) this;
		}

		public Criteria andPtrRigaIsNull() {
			addCriterion("PTR_RIGA is null");
			return (Criteria) this;
		}

		public Criteria andPtrRigaIsNotNull() {
			addCriterion("PTR_RIGA is not null");
			return (Criteria) this;
		}

		public Criteria andPtrRigaEqualTo(Long value) {
			addCriterion("PTR_RIGA =", value, "ptrRiga");
			return (Criteria) this;
		}

		public Criteria andPtrRigaNotEqualTo(Long value) {
			addCriterion("PTR_RIGA <>", value, "ptrRiga");
			return (Criteria) this;
		}

		public Criteria andPtrRigaGreaterThan(Long value) {
			addCriterion("PTR_RIGA >", value, "ptrRiga");
			return (Criteria) this;
		}

		public Criteria andPtrRigaGreaterThanOrEqualTo(Long value) {
			addCriterion("PTR_RIGA >=", value, "ptrRiga");
			return (Criteria) this;
		}

		public Criteria andPtrRigaLessThan(Long value) {
			addCriterion("PTR_RIGA <", value, "ptrRiga");
			return (Criteria) this;
		}

		public Criteria andPtrRigaLessThanOrEqualTo(Long value) {
			addCriterion("PTR_RIGA <=", value, "ptrRiga");
			return (Criteria) this;
		}

		public Criteria andPtrRigaIn(List<Long> values) {
			addCriterion("PTR_RIGA in", values, "ptrRiga");
			return (Criteria) this;
		}

		public Criteria andPtrRigaNotIn(List<Long> values) {
			addCriterion("PTR_RIGA not in", values, "ptrRiga");
			return (Criteria) this;
		}

		public Criteria andPtrRigaBetween(Long value1, Long value2) {
			addCriterion("PTR_RIGA between", value1, value2, "ptrRiga");
			return (Criteria) this;
		}

		public Criteria andPtrRigaNotBetween(Long value1, Long value2) {
			addCriterion("PTR_RIGA not between", value1, value2, "ptrRiga");
			return (Criteria) this;
		}
	}

	/**
	 * This class was generated by MyBatis Generator. This class corresponds to the database table VFIX_ASSEGNAZIONE
	 * @mbggenerated  Thu Jun 20 11:01:45 CEST 2019
	 */
	public static class Criterion {
		private String condition;
		private Object value;
		private Object secondValue;
		private boolean noValue;
		private boolean singleValue;
		private boolean betweenValue;
		private boolean listValue;
		private String typeHandler;

		public String getCondition() {
			return condition;
		}

		public Object getValue() {
			return value;
		}

		public Object getSecondValue() {
			return secondValue;
		}

		public boolean isNoValue() {
			return noValue;
		}

		public boolean isSingleValue() {
			return singleValue;
		}

		public boolean isBetweenValue() {
			return betweenValue;
		}

		public boolean isListValue() {
			return listValue;
		}

		public String getTypeHandler() {
			return typeHandler;
		}

		protected Criterion(String condition) {
			super();
			this.condition = condition;
			this.typeHandler = null;
			this.noValue = true;
		}

		protected Criterion(String condition, Object value, String typeHandler) {
			super();
			this.condition = condition;
			this.value = value;
			this.typeHandler = typeHandler;
			if (value instanceof List<?>) {
				this.listValue = true;
			} else {
				this.singleValue = true;
			}
		}

		protected Criterion(String condition, Object value) {
			this(condition, value, null);
		}

		protected Criterion(String condition, Object value, Object secondValue,
				String typeHandler) {
			super();
			this.condition = condition;
			this.value = value;
			this.secondValue = secondValue;
			this.typeHandler = typeHandler;
			this.betweenValue = true;
		}

		protected Criterion(String condition, Object value, Object secondValue) {
			this(condition, value, secondValue, null);
		}
	}

	/**
     * This class was generated by MyBatis Generator.
     * This class corresponds to the database table VFIX_ASSEGNAZIONE
     *
     * @mbggenerated do_not_delete_during_merge Tue Jun 12 11:43:04 CEST 2018
     */
    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }
}