package it.venis.iris2.oggettidbiris.dao;


import it.venis.iris2.oggettidbiris.VfixAssegnazione;
import it.venis.iris2.oggettidbiris.VfixCommenti;
import it.venis.iris2.oggettidbiris.VfixConcluse;
import it.venis.iris2.oggettidbiris.VfixFotografie;
import it.venis.iris2.oggettidbiris.VfixInopportune;
import it.venis.iris2.oggettidbiris.VfixIterSegnalazione;
import it.venis.iris2.oggettidbiris.VfixLogMimuv;
import it.venis.iris2.oggettidbiris.VfixSegnalazione;
import it.venis.iris2.oggettidbiris.VfixSegnalazioneExample;
import it.venis.iris2.oggettidbiris.VfixSegnalazioneSqlProvider;
import it.venis.iris2.oggettidbiris.VfixSegnalazione_SIT;
import it.venis.iris2.oggettidbiris.VfixSitoSegnalazione;
import it.venis.iris2.oggettidbiris.VfixUtenteRegistrato;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.annotations.Update;
import org.apache.ibatis.mapping.StatementType;
import org.apache.ibatis.type.JdbcType;


public interface  CustomMapper {

	
//	@Select("select SEQ_SERVEDURICHIESTEESENZIONI.nextval from dual");
//	@Results({
//        @Result(property = "newId", column = "newId"),
//      })
	@Select("select SEQ_SERVEDURICHIESTEESENZIONI.nextval from dual")
	Long getIdRichiesta();
	
	@Update("update SERV_EDU_RICHIESTE_ESENZIONI " + 
			" set   ESITO_NOTE = null " +
			"where  ID_RICHIESTE_ESENZIONI = #{0}" )
	void updateEsitoNoteNull(Long idRichiesta);
	

	@Update("update SERV_EDU_RICHIESTE_ESENZIONI " + 
			" set   ESITO = #{1} " +
			"where  ID_RICHIESTE_ESENZIONI = #{0}" )
	void updateEsito(Long idRichiesta, char tipoEsito);

	
	// lista delle segnalazioni...  -- PER ELASTICSEARCH
	@SelectProvider(type = CustomSqlProvider.class, method = "selectListaSegnalazioni")
	List<Map<String,String>> selectListaSegnalazioni(Long lIdSegnalazione);
	
	// iter di una segnalazione...  -- PER ELASTICSEARCH
	@SelectProvider(type = CustomSqlProvider.class, method = "selectIterSegnalazione")
	List<Map<String,String>> selectIterSegnalazione(Long lIdSegnalazione);

	// iter di una segnalazione...  -- PER ELASTICSEARCH
	@SelectProvider(type = CustomSqlProvider.class, method = "selectIterSegnalazioneMasked")
	List<Map<String,String>> selectIterSegnalazioneMasked(Long lIdSegnalazione);
	
	// insieme foto di una segnalazione...  -- PER ELASTICSEARCH
	@SelectProvider(type = CustomSqlProvider.class, method = "selectFotoSegnalazione")
	List<Map<String,String>> selectFotoSegnalazione(Long lIdSegnalazione);
	
	// lista tipi problema...
	@SelectProvider(type = CustomSqlProvider.class, method = "selectListaProblemi")
	List<Map<String,String>> selectListaProblemi(String dimeOn);

	// lista tipi stato...
	@SelectProvider(type = CustomSqlProvider.class, method = "selectListaTipoStato")
	List<Map<String,String>> selectListaTipoStato();

	// lista tipi operazioni...
	@SelectProvider(type = CustomSqlProvider.class, method = "selectListaTipoOperazioni")
	List<Map<String,String>> selectListaTipoOperazioni();
	
	// lista tipi sub stato...
	@SelectProvider(type = CustomSqlProvider.class, method = "selectListaSubStato")
	List<Map<String,String>> selectListaSubStato();	
	
	// lista tipi problema...
	@SelectProvider(type = CustomSqlProvider.class, method = "selectListaMunicipalita")
	List<Map<String,String>> selectListaMunicipalita();
	
	// lista commenti di una segnalazione...
	@SelectProvider(type = CustomSqlProvider.class, method = "selectCommentiSegnalazione")
	List<Map<String,String>> selectCommentiSegnalazione(Long lIdSegnalazione);
	
	// statistiche
	@SelectProvider(type = CustomSqlProvider.class, method = "selectStatistiche")
	List<Map<String,String>> selectStatistiche();
	
	// info ripartizione per tipologie
	@SelectProvider(type = CustomSqlProvider.class, method = "selectRipartizione")
	List<Map<String,String>> selectRipartizione();
	
	// -----	
	@SelectProvider(type = CustomSqlProvider.class, method = "insertUtente")
	@Options(statementType = StatementType.CALLABLE)
	@Results(
	{   
	 @Result(property="pID_UTENTE_REGISTRATO", column="pID_UTENTE_REGISTRATO")
	})
	List<Map<String,Short>> insertUtente(VfixUtenteRegistrato utente);	
	
	// -----
	@SelectProvider(type = CustomSqlProvider.class, method = "insertSegnalazione")
	@Options(statementType = StatementType.CALLABLE)
	@Results(
	{   
	 @Result(property="pID_SEGNALAZIONE", column="pID_SEGNALAZIONE")
	})
	List<Map<String,Short>> insertSegnalazione(VfixSegnalazione datiSegnalazione);	

	// -----	
	@SelectProvider(type = CustomSqlProvider.class, method = "insertSitoSegnalazione")
	@Options(statementType = StatementType.CALLABLE)
	List<Map<String,Short>> insertSitoSegnalazione(VfixSitoSegnalazione datiSitoSegnalazione);
	

	// -----	
	@SelectProvider(type = CustomSqlProvider.class, method = "insertFotografia")
	@Options(statementType = StatementType.CALLABLE)
	@Results(
	{   
	 @Result(property="pID_FOTOGRAFIA", column="pID_FOTOGRAFIA")
	})
	List<Map<String,Short>> insertFotografia(VfixFotografie fotografia);	
	
	
	// -----
	@SelectProvider(type = CustomSqlProvider.class, method = "insertCommento")
	@Options(statementType = StatementType.CALLABLE)
	List<Map<String,Short>> insertCommento(VfixCommenti datiCommento);	

	// -----
	@SelectProvider(type = CustomSqlProvider.class, method = "deleteCommento")
	@Options(statementType = StatementType.CALLABLE)
	List<Map<String,Short>> deleteCommento(VfixCommenti datiCommento);	

	// -----
	@SelectProvider(type = CustomSqlProvider.class, method = "rimuoviSegnalazioneLogica")
	@Options(statementType = StatementType.CALLABLE)
	List<Map<String,Short>> rimuoviSegnalazioneLogica(VfixInopportune datiInopportuna);
	
	
	// -----
	@SelectProvider(type = CustomSqlProvider.class, method = "inserisciforSIT")
	@Options(statementType = StatementType.CALLABLE)
	List<Map<String,Short>> inserisciforSIT(VfixSegnalazione_SIT theSegn);	
	

	@SelectProvider(type = CustomSqlProvider.class, method = "selectforSIT")
	@Results({
			@Result(column = "ID_SEGNALAZIONE", property = "idSegnalazione", jdbcType = JdbcType.DECIMAL),
			@Result(column = "SUBJECT", property = "subject", jdbcType = JdbcType.VARCHAR),
			@Result(column = "FK_TIPOLOGIA", property = "fkTipologia", jdbcType = JdbcType.DECIMAL),		
			@Result(column = "TIPOLOGIA", property = "tipologia", jdbcType = JdbcType.VARCHAR),	
			@Result(column = "PTR_TIPO_STATO", property = "ptrTipoStato", jdbcType = JdbcType.DECIMAL),
			@Result(column = "STATO", property = "stato", jdbcType = JdbcType.DECIMAL),
			@Result(column = "DATA_SEGNALAZIONE", property = "dataSegnalazione", jdbcType = JdbcType.TIMESTAMP),
			@Result(column = "DATA_OPER", property = "dataUltimaOperazione", jdbcType = JdbcType.TIMESTAMP),
			@Result(column = "VALIDA", property = "valida", jdbcType = JdbcType.CHAR),
			@Result(column = "LATITUDINE", property = "latitudine", jdbcType = JdbcType.DECIMAL),
			@Result(column = "LONGITUDINE", property = "longitudine", jdbcType = JdbcType.DECIMAL),
			@Result(column = "INDICAZIONI", property = "indicazioni", jdbcType = JdbcType.VARCHAR),
			@Result(column = "PTR_MUNIC", property = "ptrMunic", jdbcType = JdbcType.DECIMAL),			
			@Result(column = "COD_STATO_REALE", property = "ptrTipoStatoReale", jdbcType = JdbcType.DECIMAL),
			@Result(column = "STATO_REALE", property = "statoReale", jdbcType = JdbcType.DECIMAL)})
	
	List<VfixSegnalazione_SIT> selectforSIT(Long lIdSegnalazione);
	
	
	// -----
	@SelectProvider(type = CustomSqlProvider.class, method = "updateforSIT")
	List<Map<String,Short>> updateforSIT(VfixSegnalazione_SIT theSegn);
	
	// -----
	@SelectProvider(type = CustomSqlProvider.class, method = "deleteforSIT")
	List<Map<String,Short>> deleteforSIT(Long lIdSegnalazione);	
	
	// -----
	@SelectProvider(type = CustomSqlProvider.class, method = "getInfoSegnalazione")
	List<Map<String,String>> getInfoSegnalazione(Long lIdSegnalazione);	
		
	// -----
	@SelectProvider(type = CustomSqlProvider.class, method = "getEmailDestinatari")
	List<Map<String,String>> getEmailDestinatari(String theListaDestinatari, int idMunic);
	
	// -----
	@SelectProvider(type = CustomSqlProvider.class, method = "getListaReferenti_Ext")
	List<Map<String,String>> getListaReferenti_Ext(int theIdPrb, int theMunic, String sInfoExt);

	// -----
	@SelectProvider(type = CustomSqlProvider.class, method = "getOpenData")
	List<Map<String,String>> getOpenData();

	// -----	
	@SelectProvider(type = CustomSqlProvider.class, method = "cancellaSegnalazioneCompleta")
	@Options(statementType = StatementType.CALLABLE)
	List<Map<String,Short>> cancellaSegnalazioneCompleta(VfixSegnalazione datiSegnalazione);	

	// -----
	@SelectProvider(type = CustomSqlProvider.class, method = "getEmailSegnalatore")
	List<Map<String,String>> getEmailSegnalatore (Long theSegn);

	// -----
	@SelectProvider(type = CustomSqlProvider.class, method = "bControllaAbilitUtente")
	List<Map<String,String>> bControllaAbilitUtente (Long nSegn, String sStringaUtente, String sMunicList);
	
	// -----
	@SelectProvider(type = CustomSqlProvider.class, method = "sLeggiListaMunic")
	List<Map<String,String>> sLeggiListaMunic (String sStringaUtente);

	
	// -----	
	@SelectProvider(type = CustomSqlProvider.class, method = "updateStatoSegnalazione")
	@Options(statementType = StatementType.CALLABLE)
	List<Map<String,Short>> updateStatoSegnalazione(VfixIterSegnalazione datiIter);	

	// -----	
	@SelectProvider(type = CustomSqlProvider.class, method = "updateStatoAssegnazione")
	@Options(statementType = StatementType.CALLABLE)
	List<Map<String,Short>> updateStatoAssegnazione(VfixAssegnazione datiAssegnazione);	

	// -----	
	@SelectProvider(type = CustomSqlProvider.class, method = "insertInfoConclusa")
	@Options(statementType = StatementType.CALLABLE)
	List<Map<String,Short>> insertInfoConclusa(VfixConcluse datiConclusa);	

	// -----	
	@SelectProvider(type = CustomSqlProvider.class, method = "updateLogPrgEsterno")
	@Options(statementType = StatementType.CALLABLE)
	List<Map<String,Short>> updateLogPrgEsterno(VfixLogMimuv datiPrgEsterno);	

	// -----
	@SelectProvider(type = CustomSqlProvider.class, method = "getUltimoAssegnatario")
	@Results({
	@Result(column = "ultass", property = "ultass", jdbcType = JdbcType.DECIMAL)
	})
	Short getUltimoAssegnatario (Long theSegn);
		
	// -----
	@SelectProvider(type = CustomSqlProvider.class, method = "getUrlNotifica")
	@Results({
	@Result(column = "url_notifiche", property = "url_notifiche", jdbcType = JdbcType.VARCHAR)
	})
	String getUrlNotifica (Short nReferente);
	
	// -----
	@SelectProvider(type = CustomSqlProvider.class, method = "getDescrTipologiaProblema")
	@Results({
	@Result(column = "descrizione", property = "descrizione", jdbcType = JdbcType.VARCHAR)
	})
	String getDescrTipologiaProblema (Integer nTipo);

	// -----
	@SelectProvider(type = CustomSqlProvider.class, method = "nearestPoint")
	Map<String,String> nearestPoint (String Lat, String Lon);
	
	// -----
	@SelectProvider(type = CustomSqlProvider.class, method = "sLeggiListaMunicipalita")
	@Results({
	@Result(column = "lista_mun", property = "lista_mun", jdbcType = JdbcType.VARCHAR)
	})
	String sLeggiListaMunicipalita (String sStringaUtente);

	// -----
	@SelectProvider(type = CustomSqlProvider.class, method = "sControllaAbilitUtente")
	@Results({
	@Result(column = "ptr_refer", property = "ptr_refer", jdbcType = JdbcType.VARCHAR)
	})
	String sControllaAbilitUtente (Long nSegn, String sStringaUtente, String sMunicList);

	// -----
	@SelectProvider(type = CustomSqlProvider.class, method = "getInfoSegnalatore")
	List<Map<String,String>> getInfoSegnalatore(Long lIdSegnalazione);	
	

	// -----
	@SelectProvider(type = CustomSqlProvider.class, method = "bPrbTrattatoDaMunic")
	@Results({
	@Result(column = "quanti", property = "quanti", jdbcType = JdbcType.DECIMAL)
	})
	Short bPrbTrattatoDaMunic (Short idTipo, Short idMunic);
	
	// -----
	@SelectProvider(type = CustomSqlProvider.class, method = "desRefSpecializzatoByIdSegn")
	@Results({
	@Result(column = "txt_assegnatario", property = "txt_assegnatario", jdbcType = JdbcType.VARCHAR)
	})
	String desRefSpecializzatoByIdSegn (Long nSegn, Short nReferente);

	// -----
	@SelectProvider(type = CustomSqlProvider.class, method = "lastuserByIdSegn")
	@Results({
	@Result(column = "id_utente", property = "id_utente", jdbcType = JdbcType.VARCHAR)
	})
	String lastuserByIdSegn (Long nSegn);
	
	
	// -----
	@SelectProvider(type = CustomSqlProvider.class, method = "getInfoForUpdExternal")
	List<Map<String,String>> getInfoForUpdExternal(Long lIdSegnalazione);	

	
	// ----- Aggiornamento per Elastic Search e ITB
	@SelectProvider(type = CustomSqlProvider.class, method = "getListaSegnalazioniDaAggiornare")
	List<Map<String,String>> getListaSegnalazioniDaAggiornare();	

}

