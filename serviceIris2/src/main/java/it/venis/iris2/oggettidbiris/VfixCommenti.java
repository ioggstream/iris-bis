package it.venis.iris2.oggettidbiris;

import java.util.Date;

public class VfixCommenti extends VfixCommentiKey {

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column VFIX_COMMENTI.TESTO
	 * @mbggenerated  Thu Jun 20 11:01:45 CEST 2019
	 */
	private String testo;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column VFIX_COMMENTI.FIRMA
	 * @mbggenerated  Thu Jun 20 11:01:45 CEST 2019
	 */
	private String firma;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column VFIX_COMMENTI.DATA
	 * @mbggenerated  Thu Jun 20 11:01:45 CEST 2019
	 */
	private Date data;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column VFIX_COMMENTI.VALIDO
	 * @mbggenerated  Thu Jun 20 11:01:45 CEST 2019
	 */
	private String valido;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column VFIX_COMMENTI.TITOLO
	 * @mbggenerated  Thu Jun 20 11:01:45 CEST 2019
	 */
	private String titolo;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column VFIX_COMMENTI.INTERNO
	 * @mbggenerated  Thu Jun 20 11:01:45 CEST 2019
	 */
	private String interno;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column VFIX_COMMENTI.PTR_UTENTE
	 * @mbggenerated  Thu Jun 20 11:01:45 CEST 2019
	 */
	private String ptrUtente;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column VFIX_COMMENTI.IDEA
	 * @mbggenerated  Thu Jun 20 11:01:45 CEST 2019
	 */
	private String idea;

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column VFIX_COMMENTI.TESTO
	 * @return  the value of VFIX_COMMENTI.TESTO
	 * @mbggenerated  Thu Jun 20 11:01:45 CEST 2019
	 */
	public String getTesto() {
		return testo;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column VFIX_COMMENTI.TESTO
	 * @param testo  the value for VFIX_COMMENTI.TESTO
	 * @mbggenerated  Thu Jun 20 11:01:45 CEST 2019
	 */
	public void setTesto(String testo) {
		this.testo = testo;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column VFIX_COMMENTI.FIRMA
	 * @return  the value of VFIX_COMMENTI.FIRMA
	 * @mbggenerated  Thu Jun 20 11:01:45 CEST 2019
	 */
	public String getFirma() {
		return firma;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column VFIX_COMMENTI.FIRMA
	 * @param firma  the value for VFIX_COMMENTI.FIRMA
	 * @mbggenerated  Thu Jun 20 11:01:45 CEST 2019
	 */
	public void setFirma(String firma) {
		this.firma = firma;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column VFIX_COMMENTI.DATA
	 * @return  the value of VFIX_COMMENTI.DATA
	 * @mbggenerated  Thu Jun 20 11:01:45 CEST 2019
	 */
	public Date getData() {
		return data;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column VFIX_COMMENTI.DATA
	 * @param data  the value for VFIX_COMMENTI.DATA
	 * @mbggenerated  Thu Jun 20 11:01:45 CEST 2019
	 */
	public void setData(Date data) {
		this.data = data;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column VFIX_COMMENTI.VALIDO
	 * @return  the value of VFIX_COMMENTI.VALIDO
	 * @mbggenerated  Thu Jun 20 11:01:45 CEST 2019
	 */
	public String getValido() {
		return valido;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column VFIX_COMMENTI.VALIDO
	 * @param valido  the value for VFIX_COMMENTI.VALIDO
	 * @mbggenerated  Thu Jun 20 11:01:45 CEST 2019
	 */
	public void setValido(String valido) {
		this.valido = valido;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column VFIX_COMMENTI.TITOLO
	 * @return  the value of VFIX_COMMENTI.TITOLO
	 * @mbggenerated  Thu Jun 20 11:01:45 CEST 2019
	 */
	public String getTitolo() {
		return titolo;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column VFIX_COMMENTI.TITOLO
	 * @param titolo  the value for VFIX_COMMENTI.TITOLO
	 * @mbggenerated  Thu Jun 20 11:01:45 CEST 2019
	 */
	public void setTitolo(String titolo) {
		this.titolo = titolo;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column VFIX_COMMENTI.INTERNO
	 * @return  the value of VFIX_COMMENTI.INTERNO
	 * @mbggenerated  Thu Jun 20 11:01:45 CEST 2019
	 */
	public String getInterno() {
		return interno;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column VFIX_COMMENTI.INTERNO
	 * @param interno  the value for VFIX_COMMENTI.INTERNO
	 * @mbggenerated  Thu Jun 20 11:01:45 CEST 2019
	 */
	public void setInterno(String interno) {
		this.interno = interno;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column VFIX_COMMENTI.PTR_UTENTE
	 * @return  the value of VFIX_COMMENTI.PTR_UTENTE
	 * @mbggenerated  Thu Jun 20 11:01:45 CEST 2019
	 */
	public String getPtrUtente() {
		return ptrUtente;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column VFIX_COMMENTI.PTR_UTENTE
	 * @param ptrUtente  the value for VFIX_COMMENTI.PTR_UTENTE
	 * @mbggenerated  Thu Jun 20 11:01:45 CEST 2019
	 */
	public void setPtrUtente(String ptrUtente) {
		this.ptrUtente = ptrUtente;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column VFIX_COMMENTI.IDEA
	 * @return  the value of VFIX_COMMENTI.IDEA
	 * @mbggenerated  Thu Jun 20 11:01:45 CEST 2019
	 */
	public String getIdea() {
		return idea;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column VFIX_COMMENTI.IDEA
	 * @param idea  the value for VFIX_COMMENTI.IDEA
	 * @mbggenerated  Thu Jun 20 11:01:45 CEST 2019
	 */
	public void setIdea(String idea) {
		this.idea = idea;
	}
}